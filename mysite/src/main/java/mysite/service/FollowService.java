package mysite.service;

import java.util.List;

import javax.annotation.Resource;

import mysite.entity.Follow;

import org.seasar.extension.jdbc.JdbcManager;

public class FollowService {
	@Resource
	protected JdbcManager jdbcManager;

	public long countFollowers(String userId){
		long count=jdbcManager.from(Follow.class).where("followee_id=?",userId).getCount();
		return count;
	}

	public long countFollowees(String userId){
		long count=jdbcManager.from(Follow.class).where("follower_id=?",userId).getCount();
		return count;
	}

	public int follow(String followerId,String followeeId){
		long count=jdbcManager
				.from(Follow.class)
				.where("follower_id=? and followee_id=?",followerId,followeeId)
				.getCount();

		if(count!=0){
			return 1;
		}
		else{
			Follow follow=new Follow();
			follow.follower_id=followerId;
			follow.followee_id=followeeId;
			jdbcManager.insert(follow).execute();
			return 0;
		}
	}

	public List<Follow> findAllFolloweesById(String userId){
		List<Follow> followList=jdbcManager.from(Follow.class).where("follower_id=?",userId).getResultList();
		return followList;
	}

	public List<Follow> findAllFollowersById(String userId){
		List<Follow> followList=jdbcManager.from(Follow.class).where("followee_id=?",userId).getResultList();
		return followList;
	}
}
