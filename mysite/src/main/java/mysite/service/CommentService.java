package mysite.service;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import mysite.entity.Comment;

import org.seasar.extension.jdbc.JdbcManager;

public class CommentService {

	@Resource
	protected JdbcManager jdbcManager;

	public int insertComment(String userId,Integer tweetId,String txtComment){

		Comment comment=new Comment();

		comment.userid=userId;
		comment.tweetid=tweetId;
		comment.txtcomment=txtComment;

		Timestamp commenttime = new Timestamp(System.currentTimeMillis());
		comment.commenttime=commenttime;

		jdbcManager.insert(comment).includes("userid","tweetid","txtcomment","commenttime").execute();

		return 0;
	}

	public List<Comment> findAllByTweetId(Integer tweetId){
		List<Comment> commentList=jdbcManager
				.from(Comment.class)
				.where("tweetid=?",tweetId)
				.orderBy("commenttime desc")
				.getResultList();
		return commentList;
	}
}
