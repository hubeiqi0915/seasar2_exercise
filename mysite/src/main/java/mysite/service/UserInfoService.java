package mysite.service;

import javax.annotation.Resource;

import mysite.entity.UserInfo;

import org.seasar.extension.jdbc.JdbcManager;

public class UserInfoService {

	@Resource
	protected JdbcManager jdbcManager;

	public UserInfo getUserInfo(String id,String password){
		return jdbcManager.from(UserInfo.class).where("id=? and password=?",id,password).getSingleResult();

	}

	public boolean userIsExist(String id){
		if(jdbcManager.from(UserInfo.class).where("id=?",id).getCount()>0){
//			System.out.println(jdbcManager.from(UserInfo.class).where("id=?",id).getCount());
			return true;
		}
		else return false;
	}

	public int insertUser(String id,String password){
		UserInfo userInfo=new UserInfo();
		userInfo.id=id;
		userInfo.password=password;
		jdbcManager.insert(userInfo).includes("id","password").execute();
		return 0;
	}
}
