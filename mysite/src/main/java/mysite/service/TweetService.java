package mysite.service;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import mysite.entity.Tweet;

import org.seasar.extension.jdbc.JdbcManager;

public class TweetService {

	@Resource
	protected JdbcManager jdbcManager;

	public int insertTweet(String userId,String context){

		Tweet tweet=new Tweet();

		tweet.userid=userId;
		tweet.context=context;

		Timestamp tweettime = new Timestamp(System.currentTimeMillis());
		tweet.tweettime=tweettime;

		jdbcManager.insert(tweet).includes("userid","context","tweettime").execute();

		return 0;
	}

	public long getCountOfTweets(String userId){
		long count=0;
		try{
			count=jdbcManager.from(Tweet.class).where("userid=?",userId).getCount();
		}catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	public List<Tweet> findAllTweetsOfFollowees(String followerId){
		List<Tweet> tweetList=jdbcManager
				.from(Tweet.class)
				.innerJoin("Follow")
				.where("follower_id=? or userid=?",followerId,followerId)
				.orderBy("tweettime desc")
				.getResultList();
		return tweetList;
	}

	public List<Tweet> findAllTweetsOfUser(String userId){
		List<Tweet> tweetList=jdbcManager
				.from(Tweet.class)
				.where("userid=?",userId)
				.orderBy("tweettime desc")
				.getResultList();
		return tweetList;
	}

	public List<Tweet> getRandomTweets(String userId){
		List<Tweet> randomTweetList=jdbcManager
				.from(Tweet.class)
				.where("userid <> ?",userId)
				.orderBy("RANDOM_UUID ( )")
				.limit(5)
				.getResultList();
		return randomTweetList;
	}

	public Tweet findById(Integer id){
		Tweet tweet=jdbcManager.from(Tweet.class).where("id=?",id).getSingleResult();
		return tweet;
	}

//	public long countAllTweetsOfUser(String userId){
//		long count=this.jdbcManager.from(Tweet.class).where("userid=?",userId).getCount();
//		return count;
//	}
}
