package mysite.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mysite.dto.UserDto;
import mysite.entity.Comment;
import mysite.entity.Follow;
import mysite.entity.Tweet;
import mysite.form.TweetForm;
import mysite.service.CommentService;
import mysite.service.FollowService;
import mysite.service.TweetService;

import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

public class MainAction {

	public String userId="";
	public String err_msg="";
	public String c_err_msg="";
	public long countOfTweets;
	public long countOfFollowees;
	public long countOfFollowers;
	public String account;
	public String tag="home";

	public boolean hasNext=false;
	public boolean hasPrev=false;
	public List<Tweet> tweetList;
	public List<Tweet> randomTweetList;
	public List<Follow> followList;
	public List<Comment> commentList;
	public Tweet detail;

	@Resource
	@ActionForm
	protected TweetForm tweetForm;

	@Resource
	protected HttpSession session;
	@Resource
	protected HttpServletRequest request;
	@Resource
	protected HttpServletResponse response;

	@Resource
	protected UserDto userDto;

	@Resource
	protected TweetService tweetService;
	@Resource
	protected FollowService followService;
	@Resource
	protected CommentService commentService;

	@Resource
	protected JdbcManager jdbcManager;

	@Execute(validator=false)
	public String index(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;
		this.countOfTweets=tweetService.getCountOfTweets(userId);
		this.countOfFollowees=followService.countFollowees(userId);
		this.countOfFollowers=followService.countFollowers(userId);

		this.randomTweetList=this.tweetService.getRandomTweets(userId);

		if(this.tag.equals("home")){
			this.tweetList=this.tweetService.findAllTweetsOfFollowees(userId);
		}
		else if(this.tag.equals("profile")){
			this.tweetList=this.tweetService.findAllTweetsOfUser(userId);
		}
		else if(this.tag.equals("detail")){
			Integer tweetId=Integer.parseInt(request.getParameter("tweetId"));
			this.detail=this.tweetService.findById(tweetId);
			this.commentList=this.commentService.findAllByTweetId(tweetId);
		}

		return "main.jsp";
	}

	@Execute(validator=true,input="main.jsp")
	public String tweet(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.countOfTweets=tweetService.getCountOfTweets(userId);
		this.userId=userDto.id;
		if(!tweetForm.tweetinfo.context.equals("")){
			tweetService.insertTweet(userId, tweetForm.tweetinfo.context);
			return "/main/?redirect=true";
		}
		else{
			this.err_msg="Tweet cannot be blank!";
			return "/main/index";
		}
	}

	@Execute(validator=false)
	public String showFollowees(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;

		this.tag="showFollowees";

		this.countOfFollowees=followService.countFollowees(userId);
		this.followList=this.followService.findAllFolloweesById(userId);
		return "/main/index";
	}

	@Execute(validator=false)
	public String showFollowers(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;

		this.tag="showFollowers";

		this.countOfFollowers=followService.countFollowers(userId);
		this.followList=this.followService.findAllFollowersById(userId);
		return "/main/index";
	}

	@Execute(validator=false)
	public String follow(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;

		String followeeId=request.getParameter("followeeId");
//		String followeeId=followForm.followeeId;
		this.followService.follow(userId, followeeId);

		return "/main/index";
	}

	@Execute(validator=false)
	public String show(){

		this.tag="show";
		this.account=request.getParameter("account");

		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;

		this.tweetList=this.tweetService.findAllTweetsOfUser(this.account);

		return "/main/index";
	}

	@Execute(validator=false)
	public String home(){
//		this.tagUrl="home.jsp";
		this.tag="home";
		return "/main/index";
	}

	@Execute(validator=false)
	public String messages(){
		return null;
	}

	@Execute(validator=false)
	public String profile(){
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}
		this.userId=userDto.id;

//		/*paging*/
//		long total=this.tweetService.getCountOfTweets(userId);
//		int page=IntegerConversionUtil.toPrimitiveInt(this.tweetListForm.page);
//		this.tweetList=this.tweetService.findAllTweetsOfUser(userId, page, LIMIT);
//		if(page!=0){
//			hasPrev=true;
//		}
//		if((page+1)*LIMIT<total){
//			hasNext=true;
//		}
//		this.tagUrl="/WEB-INF/view/main/profile.jsp?page="+(page+1);
		this.tag="profile";
//		this.tagUrl="profile.jsp";
		return "/main/index";
	}


	@Execute(validator=false)
	public String detail(){
		this.tag="detail";

		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}

		this.userId=userDto.id;

		Integer tweetId=Integer.parseInt(request.getParameter("tweetId"));
		this.detail=tweetService.findById(tweetId);
		this.commentList=commentService.findAllByTweetId(tweetId);

		return "/main/index";
	}

	@Execute(validator=false)
	public String comment(){
		this.tag="detail";
		userDto=(UserDto)session.getAttribute("userDto");
		if(userDto==null){
			return "login/login";
		}

		this.userId=userDto.id;

		Integer tweetid=tweetForm.commentinfo.tweetid;
		String txtcomment=tweetForm.commentinfo.txtcomment;


		if(!txtcomment.equals("")){
			commentService.insertComment(userId, tweetid,txtcomment);

//			return "/main/?redirect=true";
			return "/main/detail?tweetId="+tweetid;
		}
		else{
			this.c_err_msg="Comment cannot be blank!";
//			return "/main/index";
			return "/main/detail?tweetId="+tweetid;
		}

//		return "/main/index";
	}
}
