package mysite.action;

import javax.annotation.Resource;

import mysite.dto.UserDto;
import mysite.form.RegisterForm;
import mysite.service.UserInfoService;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

public class RegisterAction {

	public String message="";

	@Resource
	@ActionForm
	protected RegisterForm registerForm;

	@Resource
	protected UserDto userDto;

	@Resource
	protected UserInfoService userInfoService;

	@Execute(validator=false)
	public String index(){
		return "register.jsp";
	}

//	@Execute(validate="validate", input="register.jsp")
	@Execute(validator=true,input="register.jsp")
	public String register(){
		boolean userIsExist=userInfoService.userIsExist(registerForm.id);
		if(!userIsExist){
			userInfoService.insertUser(registerForm.id, Encode.encodeByMD5(registerForm.password));
			userDto.id=registerForm.id;
			this.message="";
			return "/main/index";
		}
		else{
			this.message="This id is already exist.";
			return "/register/register.jsp";
		}
	}
}
