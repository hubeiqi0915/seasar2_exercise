package mysite.action;

import javax.annotation.Resource;

import mysite.dto.UserDto;
import mysite.entity.UserInfo;
import mysite.form.LoginForm;
import mysite.service.UserInfoService;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

public class LoginAction {

	public String message="Please enter your id and password.";

	@Resource
	@ActionForm
	protected LoginForm loginForm;

	@Resource
	protected UserDto userDto;

	@Resource
	protected UserInfoService userInfoService;

	@Execute(validator=false)
	public String index(){
		return "login.jsp";
	}

	@Execute(validator=true,input="login.jsp")
	public String login(){
		UserInfo userInfo=userInfoService.getUserInfo(loginForm.id,Encode.encodeByMD5(loginForm.password));
		if(userInfo!=null){
			userDto.id=userInfo.id;
			return "/main/index";
		}
		else{
			this.message="Login Error!Please check your id or password.";
			return "/login/login.jsp";
		}
	}

	@Execute(validator=false)
	public String logout(){
		userDto.id=null;
		this.message="Please enter your id and password.";
		return "/login/?redirect=true";
	}

	@Execute(validator=false)
	public String register(){
		return "/register/register.jsp";
	}
}


