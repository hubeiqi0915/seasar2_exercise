package mysite.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tweet")
public class Tweet {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	public Integer id;

	@Column(name="userid")
	public String userid;

	@Column(name="context")
	public String context;

	@Column(name="tweettime")
	public Timestamp tweettime;

	@ManyToOne
	@JoinColumn(name="userid",referencedColumnName="followee_id")
	public Follow follow;

}
