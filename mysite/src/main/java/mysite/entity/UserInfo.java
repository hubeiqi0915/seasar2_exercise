package mysite.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_info")
public class UserInfo {

	@Id
	@Column(name="id")
	public String id;
	@Column(name="password")
	public String password;
}