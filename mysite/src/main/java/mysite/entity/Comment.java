package mysite.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="comment")
public class Comment {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	public Integer id;

	@Column(name="userid")
	public String userid;

	@Column(name="tweetid")
	public Integer tweetid;

	@Column(name="txtcomment")
	public String txtcomment;

	@Column(name="commenttime")
	public Timestamp commenttime;

}
