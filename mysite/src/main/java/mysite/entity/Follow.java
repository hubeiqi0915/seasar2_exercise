package mysite.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="follow")
public class Follow {

	@Column(name="follower_id")
	public String follower_id;
	@Column(name="followee_id")
	public String followee_id;

}
