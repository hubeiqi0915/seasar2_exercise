package mysite.form;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;
import org.seasar.struts.annotation.Required;

@Component(instance=InstanceType.SESSION)
public class LoginForm implements Serializable{

	private static final long serialVersionUID=1L;

	@Required
	public String id;
	@Required
	public String password;

	public void reset(){
		id="";
		password="";
	}

//	public User user;
//
//	public static class User{
//		public String id;
//		public String password;
//	}
}
