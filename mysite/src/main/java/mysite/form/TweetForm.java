package mysite.form;

public class TweetForm{

//	private static final long serialVersionUID=1L;

//	@Required
//	public String context;
//
//	public void reset(){
//		context="";
//	}

	public Tweetinfo tweetinfo=new Tweetinfo();
	public Commentinfo commentinfo=new Commentinfo();

	public static class Tweetinfo{
		public String context;
	}

	public static class Commentinfo{
		public Integer tweetid;
		public String txtcomment;
	}

	public void reset(){
		tweetinfo.context="";
		commentinfo.txtcomment="";
	}
}
