package mysite.form;

import java.io.Serializable;

import org.seasar.struts.annotation.Required;
import org.seasar.struts.annotation.Validwhen;

public class RegisterForm implements Serializable{

	private static final long serialVersionUID=1L;

	@Required
	public String id;
	@Required
	public String password;
	@Required
	@Validwhen(test="(password == confirm_password)",msg=@org.seasar.struts.annotation.Msg(key = "Please confirm your password.",resource=false))
	public String confirm_password;

//	public ActionMessages validate(){
//		ActionMessages errors=new ActionMessages();
//		if(!confirm_password.equals(password)){
//			errors.add("confirm_password",new ActionMessage("Please confirm your password.",false));
//		}
//		return errors;
//	}

	public void reset(){
		id="";
		password="";
		confirm_password="";
	}

}
