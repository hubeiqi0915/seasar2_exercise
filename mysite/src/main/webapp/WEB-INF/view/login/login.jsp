<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<title>Login</title>
<%@ include file="/WEB-INF/view/common/header.jsp"%>
</head>
<body>

	<s:link href="logout/">Logout</s:link>
	<br>

	<p>
		<font color="red">${message}</font>
	</p>

	<s:form method="POST">
		<table>
			<tr>
				<td>User ID:</td>
				<td><html:text property="id" /></td>
				<td><html:errors property="id" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><html:password property="password" /></td>
				<td><html:errors property="password" /></td>
			</tr>
		</table>
		<s:submit property="login" value="Login"></s:submit>
	</s:form>
	<s:link href="register/">Register</s:link>

</body>
</html>