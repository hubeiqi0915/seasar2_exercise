<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<title>Register</title>
<%@ include file="/WEB-INF/view/common/header.jsp"%>
</head>
<body>
	<s:form method="POST">
		<table>
			<tr>
				<td>User ID:</td>
				<td><html:text property="id" /></td>
				<td><html:errors property="id" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><html:password property="password" /></td>
				<td><html:errors property="password" /></td>
			</tr>
			<tr>
				<td>Confirm password:</td>
				<td><html:password property="confirm_password" /></td>
				<td><html:errors property="confirm_password" /></td>
			</tr>
		</table>
		<s:submit property="register" value="Register"></s:submit>
	</s:form>
</body>
</html>