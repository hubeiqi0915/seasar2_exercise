<%@ page pageEncoding="UTF-8"%>
<html>
<head>
<title>Home</title>
<%@ include file="/WEB-INF/view/common/header.jsp"%>
<script type="text/javascript" src="${f:url('/js/jquery-1.11.2.js')}"></script>
<script type="text/javascript">
	function comment(tweetId){
// 		$("[name='comment']").attr("href","/main/comment?tweetid="+tweetid+"&txtcomment="+$('#txtcomment').val());
		alert($("[name='comment']").href);
	}
</script>
</head>
<body>
	<s:link href="/login/logout/">Logout</s:link>
	<br>
	<table width="100%">
		<tr>

			<td rowspan="2" width="20%" height="800" valign="top"
				bgcolor="#DDDDDD">
				<table border="1" width="100%">
					<tr>
						<td align="center"><strong>${userId}</strong></td>
					</tr>
					<tr>
						<td>Tweets: <strong><s:link href="/main/profile/">${countOfTweets}</s:link></strong>
						</td>
					</tr>
					<tr>
						<td>Follow <strong><s:link href="/main/showFollowees/">${countOfFollowees}</s:link></strong> accounts.
						</td>
					</tr>
					<tr>
						<td><strong><s:link href="/main/showFollowers/">${countOfFollowers}</s:link></strong> accounts follow me.</td>
					</tr>
				</table>
				<ul>
					<li><s:link href="home/">Home</s:link></li>
					<li>Messages</li>
					<li><s:link href="profile/">Profile</s:link></li>
				</ul>
			</td>
			<td width="60%" valign="top">
				<%-- 				<%@ include file="/WEB-INF/view/main/home.jsp" %> --%> <%-- 				<c:import url= "${tagUrl}"></c:import> --%>

<!-- show followees -->
				<c:if test="${tag=='showFollowees'}">
					<div>
						Follow <strong>${countOfFollowees}</strong> accounts.
					</div>
					<s:form method="POST" action="show">
						<ul>
							<c:forEach var="follow" items="${followList}">
								<li><s:link href="/main/show?account=${follow.followee_id}">${follow.followee_id}</s:link></li>
							</c:forEach>
						</ul>
					</s:form>
				</c:if>

<!-- show followers -->
				<c:if test="${tag=='showFollowers'}">
					<div>
						<strong>${countOfFollowers}</strong> accounts follow me.
					</div>
					<s:form method="POST" action="show">
						<ul>
							<c:forEach var="follow" items="${followList}">
								<li><s:link href="/main/show?account=${follow.follower_id}">${follow.follower_id}</s:link></li>
							</c:forEach>
						</ul>
					</s:form>
				</c:if>

<!-- home -->
				<c:if test="${tag=='home'}">
					<div align="center">
					<s:form method="POST">
						<table>
							<tr>
								<td colspan="2"><html:textarea property="tweetinfo.context" cols="60" value=""
										rows="5"></html:textarea></td>
							</tr>
							<tr>
								<td><font color="red">${err_msg}</font></td>
								<%--							<td><font color="red"><html:errors property="context"/></font></td> --%>
								<td align="right"><s:submit property="tweet" value="Tweet"></s:submit></td>
							</tr>
						</table>
					</s:form>
					</div>
					<%-- 					${userId} --%>
					<div align="center">
						<s:form method="POST">
							<c:forEach var="tweet" items="${tweetList}">
								<table width="95%">
									<tr>
										<td colspan="2">
											<h2><s:link href="/main/show?account=${tweet.userid}">${tweet.userid}</s:link></h2>
										</td>
									</tr>
									<tr>
										<td colspan="2"><h3>${tweet.context}</h3></td>
									</tr>
									<tr>
										<td width="50%" align="left">${tweet.tweettime}</td>
										<td align="right">
<!-- 											<input type="button" value="comments" class="btnComment"	onclick="$('#${tweet.id}').toggle();"/> -->
											<s:link href="/main/detail?tweetId=${tweet.id}">Detail</s:link>
										</td>
									</tr>
<!-- 									<tr> -->
<!-- 										<td colspan="2"><hr color="#DDDDDD"></td> -->
<!-- 									</tr> -->
								</table>
<%-- 								<div id="${tweet.id}" class="comments" style="display: none;"> --%>
<!-- 									<hr color="#DDDDDD" width=96%> -->
<!-- 								</div> -->
								<hr>
							</c:forEach>
						</s:form>
					</div>
				</c:if>

<!-- detail -->
				<c:if test="${tag=='detail'}">
					<div align="center">
						<table width=95%>
							<tr>
								<td colspan="2">
									<h2><s:link href="/main/show?account=${detail.userid}">${detail.userid}</s:link></h2>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>${detail.context}</h3></td>
							</tr>
							<tr>
								<td width="50%" align="left">${detail.tweettime}</td>
								<td align="right">
								</td>
							</tr>
						</table>
						<hr width=95%>
						<table width=95%>
							<c:forEach var="comment" items="${commentList}">
								<tr>
									<td>
										<s:link href="/main/show?account=${comment.userid}">${comment.userid}</s:link>:${comment.txtcomment}
									</td>
								</tr>
								<tr>
									<td align="left"><font size="1">${comment.commenttime}</font></td>
								</tr>
								<tr>
									<td><hr color="#DDDDDD" width="95%"></td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<div align="center">
						<s:form method="POST">
<!-- 							<form action="comment" method="post"> -->
							<table>
								<tr>
									<td colspan="2">
										<html:hidden property="commentinfo.tweetid" value="${detail.id}"/>
										<html:textarea property="commentinfo.txtComment" cols="60" rows="1" value=""></html:textarea>
<!-- 										<textarea id="txtcomment" cols="60" rows="1"></textarea> -->
									</td>
								</tr>
								<tr>
									<td><font color="red">${c_err_msg}</font></td>
									<td align="right">
<%-- 										<s:link name="comment" href="/main/comment?tweetid=${detail.id}&txtcomment=$('#txtcomment').val()" onclick="window.location.href=/main/comment?tweetid=${detail.id}&txtcomment=$('#txtcomment').val()">Comment</s:link> --%>
<%-- 										<input type="button" onclick="window.location.href='/main/comment?tweetid=${detail.id}&txtcomment=$('#txtcomment')';" value="Comment"/> --%>
											<s:submit property="comment" value="Comment"></s:submit>
									</td>
								</tr>
							</table>
<!-- 							</form> -->
						</s:form>
					</div>
				</c:if>

<!-- show one's profile-->
				<c:if test="${tag=='show'}">
					<h2><strong>${account}</strong></h2>
					<div align="left" >
						<s:form method="POST" action="follow">
							<s:link href="/main/follow?followeeId=${account}">Follow</s:link>
						</s:form>
					</div>
					<div align="center">
						<c:forEach var="tweet" items="${tweetList}">
							<table width="95%">
								<tr>
									<td colspan="2"><h3>${tweet.context}</h3></td>
								</tr>
								<tr>
									<td width="50%" align="left">${tweet.tweettime}</td>
									<td align="right"><s:link href="/main/detail?tweetId=${tweet.id}">Detail</s:link></td>
								</tr>
								<tr>
									<td colspan="2"><hr></td>
								</tr>
							</table>
						</c:forEach>
						<%-- 					<c:if test="${hasPrev}"> --%>
						<%-- 						<a href="?page=${page-1}">&lt;Previous</a> --%>
						<%-- 					</c:if> --%>
						<%-- 					<c:if test="${hasNext}"> --%>
						<%-- 						<a href="?page=${page+1}">Next&gt;${page}</a> --%>
						<%-- 					</c:if> --%>
					</div>
				</c:if>

<!-- profile -->
				<c:if test="${tag=='profile'}">
						<h2><strong>${userId}</strong></h2>
					<div align="center">
						<c:forEach var="tweet" items="${tweetList}">
							<table width="95%">
								<tr>
									<td colspan="2"><h3>${tweet.context}</h3></td>
								</tr>
								<tr>
									<td width="50%" align="left">${tweet.tweettime}</td>
									<td align="right"><s:link href="/main/detail?tweetId=${tweet.id}">Detail</s:link></td>
								</tr>
								<tr>
									<td colspan="2"><hr></td>
								</tr>
							</table>
						</c:forEach>
						<%-- 					<c:if test="${hasPrev}"> --%>
						<%-- 						<a href="?page=${page-1}">&lt;Previous</a> --%>
						<%-- 					</c:if> --%>
						<%-- 					<c:if test="${hasNext}"> --%>
						<%-- 						<a href="?page=${page+1}">Next&gt;${page}</a> --%>
						<%-- 					</c:if> --%>
					</div>
				</c:if>

			</td>

			<td rowspan="2" width="20%" height="800" valign="top" bgcolor="#DDDDDD" align="center">
				<div align="center"><h3>Recommend</h3></div>
				<table width="90%">
					<s:form method="POST" action="follow">
						<c:forEach var="randomTweet" items="${randomTweetList}">
							<tr>
								<td>
								<strong>${randomTweet.userId}</strong><br>
								<font size="1px">
									<s:link href="/main/follow?followeeId=${randomTweet.userId}">Follow</s:link>
								</font><br>
								${randomTweet.context}
								</td>
							</tr>
						</c:forEach>
					</s:form>
				</table>
			</td>
		</tr>
		<tr></tr>
	</table>

<!-- functions -->


</body>
</html>